package com.KPLC.Bills;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Hashtable;

import org.apache.log4j.Logger;
import org.json.JSONObject;
import org.springframework.http.ResponseEntity;

import com.KPLC.comm.DbManager;
import com.KPLC.comm.GeneralCodes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;


public class HitKPLC {
	static Connection conn;
	static Logger log = Logger.getLogger(HitKPLC.class.getName());

	public static String KPLCqueryBills(String accountRefNo,String biller_code,String file_id,String alias) {
		String status="Validated";
		String bal="0.00";
		String account_name="0";
		String due_date="0-0-0";
		String account_no=accountRefNo;

		try {
		 Hashtable<String,String>map=new Hashtable<String,String>();
		 map.put("AccountRefNo",accountRefNo);
		 map.put("BillerCode",biller_code);
		 map.put("BillerUrl","KPLC_URL");
		 loggAccountValidation("REQUEST",biller_code, map.toString(), log);
		 String response=GeneralCodes.encryptedPostRequest(map, ExternalFile.getUsbBaseUrl(), "JSON");
		loggAccountValidation("RESPONSE",biller_code, response, log);
		 JSONObject obj = new JSONObject(response);
         String jsonResponse = obj.toString();
       Gson gson = new GsonBuilder().create();
       FetchBillsResponse res = gson.fromJson(jsonResponse, FetchBillsResponse.class);
  
       if(res.getResponseCode().isEmpty() || res.getResponseCode().equalsIgnoreCase("")) {
			 bal="Notfound";
		    account_name="Not found";
		    due_date="Not found";
		    account_no="Not found";
		 }
       else if(res.getResponseCode().equalsIgnoreCase("0")) {
			 if(res.getDueDate().isEmpty() || res.getDueDate().equalsIgnoreCase("")) {
				 due_date="0000-00-00";
			 }
			 else {
				 due_date=res.getDueDate();
			 }
			 account_name=res.getBillDescription();
			 bal=res.getBillBalance().replaceAll(",", "");
			 account_no=res.getAccountRefNo();
	     
			 
		 }
       else if(res.getResponseCode().equalsIgnoreCase("500") || res.getResponseCode().equalsIgnoreCase("300") || res.getResponseCode().equalsIgnoreCase("301")) {
    	   bal="Failure";
		    account_name="Failure";
		    due_date="Failure";
		    account_no="Failure"; 
       }
       else {
    	   System.out.println("Response EError");
			    bal="Error";
			    account_name="Error";
			    due_date="Error";
			    account_no="Error"; 
		 }
     
		}catch(Exception n) {
			loggAccountValidation("RESPONSE",biller_code, "Error Code 500 occured", log);
//			n.printStackTrace();
			bal="Error";
		    account_name="Error";
		    due_date="Error";
		    account_no="Error";
		}

		return status+"*"+bal+"*"+account_name+"*"+due_date+"*"+account_no+"*"+alias;
		//return status+"*"+"27.65"+"*"+"CLINTON ONYANGO ORANG OTAN"+"*"+"25-10-2020"+"*"+account_no+"*"+alias;

	}
	
	 public static void loggAccountValidation(String type,String email, String description, Logger log) {
		 log.info(type+" "+email + " " + description +  " at " + new java.util.Date());
		 
	 }
	
	
	 
	 
	 public static void saveCroneLogs(String job_type,String time,String job_name,String accounts,String status,String time_started,String time_ended,String payer_code) {
	     String 	 updated_accounts="0";
		 try {
			 conn = DbManager.getConnection();
			 String sql1="SELECT COUNT(*)vcount FROM account WHERE DATE(updated_at) = CURDATE() AND updated=?";
				PreparedStatement prep2 = conn.prepareStatement(sql1);
				prep2.setObject(1, "YES");
			    ResultSet rs=prep2.executeQuery();
			    while(rs.next()) {
			    	updated_accounts=rs.getString("vcount");
			    }
			 
				String sql = "INSERT INTO crone_logs(job_type,time,job_name,accounts,status,time_started,time_ended,updated_accounts)VALUES(?,?,?,?,?,?,?,?)";
				PreparedStatement prep = conn.prepareStatement(sql);
				prep.setObject(1, job_type);
				prep.setObject(2, time);
				prep.setObject(3, job_name);
				prep.setObject(4, accounts);
				prep.setObject(5,status );
				prep.setObject(6, time_started);
				prep.setObject(7, time_ended);
				prep.setObject(8, updated_accounts);
				prep.execute();
				
				String sql3 = "UPDATE account set started=?,updated=? WHERE updated=?";
				PreparedStatement prep3 = conn.prepareStatement(sql3);
				prep3.setObject(1, "NO");
				prep3.setObject(2, "NO");
				prep3.setObject(3, "NO");
				prep3.execute();
				
				conn.close();
			 
		 }catch(Exception e) {
			 e.printStackTrace();
		 }
		 
		 
	 }
}
