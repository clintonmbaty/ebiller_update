package com.ebiller_updates.api;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Hashtable;
import java.util.Vector;
import java.util.concurrent.CountDownLatch;

import org.json.JSONObject;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.KPLC.Crone.CroneHelper;
import com.KPLC.comm.DESEncryption;
import com.KPLC.comm.DbManager;
import com.ebiller_updates.Work.AutoUpdateKplcPayment;


@SpringBootApplication
@RequestMapping("/common") 
public class api {
	static Connection conn = null;
	@RequestMapping(value = "/v1/updateMyAccounts", method = RequestMethod.POST)
	ResponseEntity<?> updateMyAccounts(@RequestBody String data) {
		System.out.println("Individual trying to update accounts>>>>>>>>>>>>>>>>>");
		
		 CountDownLatch latchl1 = new CountDownLatch(1);
			Runnable runl1 = new AutoUpdateKplcPayment(latchl1);
			Thread tl1 = new Thread(runl1);
			tl1.start();
		try {
			    data=DESEncryption.decrypt(data);
			    System.out.println(data);
				JSONObject obj = new JSONObject(data);
				String biller_code=obj.getString("biller_code");
				String payer_code=obj.getString("payer_code");
				 Vector vector = new Vector();
				 String sql="SELECT  account_no FROM `account` WHERE biller_code=? AND payer_code=?";
				 conn=DbManager.getConnection();
					PreparedStatement prep=conn.prepareStatement(sql);
					prep.setObject(1, biller_code);
					prep.setObject(2, payer_code);
					ResultSet rs=prep.executeQuery();
					while(rs.next()) {
						Vector holder = new Vector();
						holder.addElement(rs.getString("account_no"));
						vector.addElement(holder);
					}
					conn.close();
					System.out.println("Accounts Found>>>>>>>>>"+vector.size());
					if(vector.isEmpty()) {
						
					}else {
						 Thread t = new Thread() {
								public void run() {
					CroneHelper.QueryBills(vector);
								}
							};
							t.start(); 	
					}
		}catch(Exception e) {
			e.printStackTrace();
		}
		
		Hashtable<Object, Object> map = new Hashtable<Object, Object>();
		map.put("responseCode", "00");
		map.put("responseMessage", "Update started successfully");
		
		return ResponseEntity.ok(map);
	}
	
	@RequestMapping(value = "/v1/updatAllAccounts", method = RequestMethod.POST)
	ResponseEntity<?> updatAllAccounts(@RequestBody String data) {
		System.out.println("All to update all accounts>>>>>>>>>>>>>>>>>");
		 CountDownLatch latchl2 = new CountDownLatch(1);
			Runnable runl2 = new AutoUpdateKplcPayment(latchl2);
			Thread tl2 = new Thread(runl2);
			tl2.start();
			
		try {
				 Vector vector = new Vector();
				 String sql="SELECT  account_no FROM `account` ORDER BY updated_at ASC";
				 conn=DbManager.getConnection();
					PreparedStatement prep=conn.prepareStatement(sql);
					ResultSet rs=prep.executeQuery();
					while(rs.next()) {
						Vector holder = new Vector();
						holder.addElement(rs.getString("account_no"));
						vector.addElement(holder);
					}
					conn.close();
					System.out.println(" All Accounts Found>>>>>>>>>"+vector.size());
					if(vector.isEmpty()) {
						
					}else {
						 Thread t = new Thread() {
								public void run() {
					CroneHelper.QueryBills(vector);
								}
							};
							t.start(); 	
					}
						
		}catch(Exception e) {
			e.printStackTrace();
		}
		
		Hashtable<Object, Object> map = new Hashtable<Object, Object>();
		map.put("responseCode", "00");
		map.put("responseMessage", "Update started successfully");
		
		
			
		return ResponseEntity.ok(map);
	}


}
