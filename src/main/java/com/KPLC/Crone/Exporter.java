package com.KPLC.Crone;

import java.io.FileOutputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import com.KPLC.Bills.ExternalFile;
import com.KPLC.Bills.HitKPLC;
import com.KPLC.comm.DbManager;
import com.KPLC.comm.Error_Logs;


public class Exporter {
	static Connection conn=null;
	
	public static void startExport() {
		
		 long startTime = System.currentTimeMillis(); // Get the start Time
	        long endTime = 0;
	        long time2=0;
	        
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		LocalDateTime now = LocalDateTime.now();
		String time = dtf.format(now);

		try {
			 conn=DbManager.getConnection();
		 String sql="SELECT *,\r\n" + 
		 		"(SELECT bank_ref_no FROM eslip B WHERE  B.eslip_no=A.eslip_no)bank_ref_no\r\n" + 
		 		"FROM `eslip_bills` A WHERE status='PAID' AND  updated_at >=?\r\n" + 
		 		"\r\n" + 
		 		"";
		 PreparedStatement prep=conn.prepareStatement(sql);
		 prep.setObject(1, time);
		 ResultSet rs=prep.executeQuery();
		 
		 XSSFWorkbook workbook = new XSSFWorkbook();
         XSSFSheet sheet = workbook.createSheet("Reports");

         writeHeaderLine(sheet);

         writeDataLines(rs, workbook, sheet);

         FileOutputStream outputStream = new FileOutputStream(ExternalFile.getExcelReportURL()+"/Ebiller_Report_"+time.replace("/", "_")+".xlsx");
         FileOutputStream outputStream_team = new FileOutputStream(ExternalFile.getExcelReportURL_Team()+"Ebiller_Report_"+time.replace("/", "_")+".xlsx");
         workbook.write(outputStream);
         workbook.write(outputStream_team);
         workbook.close();
		 conn.close();
		 
		 
		 endTime = System.currentTimeMillis(); //Get the end Time
		 time2=(endTime-startTime);
		 HitKPLC.saveCroneLogs("CRONE JOB", time2+"", "EXCEL REPORT", "NA", "SUCCESS", startTime+"", endTime+"","NA");
		}catch(Exception e) {
			e.printStackTrace();
			 Error_Logs.LogExceptions(e.toString(),"NA", e.getClass().toString(), CroneHelper.class.getName().toString());
		}
		
	}
	
	
	private static void writeHeaderLine(XSSFSheet sheet) {
		 
        Row headerRow = sheet.createRow(0);
 
        Cell headerCell = headerRow.createCell(0);
        headerCell.setCellValue("Date Created");
 
        headerCell = headerRow.createCell(1);
        headerCell.setCellValue("Eslip Number ");
 
        headerCell = headerRow.createCell(2);
        headerCell.setCellValue("Account Number");
 
        headerCell = headerRow.createCell(3);
        headerCell.setCellValue("Amount Due");
 
        headerCell = headerRow.createCell(4);
        headerCell.setCellValue("Amount to Pay");
        
        headerCell = headerRow.createCell(5);
        headerCell.setCellValue("Status");
        
        headerCell = headerRow.createCell(6);
        headerCell.setCellValue("Due Date");
        
        headerCell = headerRow.createCell(7);
        headerCell.setCellValue("Payment Date");
        
        headerCell = headerRow.createCell(8);
        headerCell.setCellValue("Bank ref no");
        
        headerCell = headerRow.createCell(9);
        headerCell.setCellValue("Biller Ref no");
        
        headerCell = headerRow.createCell(10);
        headerCell.setCellValue("Account Name");
        
        headerCell = headerRow.createCell(11);
        headerCell.setCellValue("Date");
    }
	
	private static void writeDataLines(ResultSet result, XSSFWorkbook workbook, XSSFSheet sheet)  {
       try {
		int rowCount = 1;
        while (result.next()) {
        	SimpleDateFormat df = new SimpleDateFormat("yyyy/MM/dd");
            String date_created = result.getString("created_at");
            String eslip_no = result.getString("eslip_no");
            String  account_no = result.getString("account_no");
            String amount_due = result.getString("amount_due");
            String amount_to_pay = result.getString("amount_to_pay");
            String status = result.getString("status");
            String due_date = result.getString("due_date");
            String payment_date =result.getString("updated_at");
            String bank_ref_no =result.getString("bank_ref_no");
            String biller_ref =result.getString("biller_payment_ref");
            String account_name =result.getString("account_name");
            Timestamp timestamp = result.getTimestamp("created_at");

           Row row = sheet.createRow(rowCount++);
 
            int columnCount = 0;
            Cell cell = row.createCell(columnCount++);
            cell.setCellValue(date_created);
 
            cell = row.createCell(columnCount++);
            cell.setCellValue(eslip_no);
            
            cell = row.createCell(columnCount++);
            cell.setCellValue(account_no);
 
            cell = row.createCell(columnCount++);
            cell.setCellValue(amount_due);
            
            
            cell = row.createCell(columnCount++);
            cell.setCellValue(amount_to_pay);
            
            cell = row.createCell(columnCount++);
            cell.setCellValue(status);
            
            cell = row.createCell(columnCount++);
            cell.setCellValue(due_date);
           
 
            cell = row.createCell(columnCount++);
            cell.setCellValue(payment_date);
            
            cell = row.createCell(columnCount++);
            cell.setCellValue(bank_ref_no);
            
            cell = row.createCell(columnCount++);
            cell.setCellValue(biller_ref);
            
            cell = row.createCell(columnCount++);
            cell.setCellValue(account_name);
            

            cell = row.createCell(columnCount++);
            CellStyle cellStyle = workbook.createCellStyle();
            CreationHelper creationHelper = workbook.getCreationHelper();
            cellStyle.setDataFormat(creationHelper.createDataFormat().getFormat("yyyy-MM-dd"));
            cell.setCellStyle(cellStyle);
            cell.setCellValue(timestamp);
 
 
        }
       }catch(Exception e) {
    	   Error_Logs.LogExceptions(e.toString(),"NA", e.getClass().toString(), CroneHelper.class.getName().toString());
    	   e.printStackTrace();
       }
    }
}
