package com.ebiller_updates.Work;

import java.util.concurrent.CountDownLatch;

public class AutoUpdateKplcPayment  implements Runnable {
	private CountDownLatch latch;
	public boolean execute = true;

	public AutoUpdateKplcPayment(CountDownLatch latch) {
		this.latch = latch;
	}
	@SuppressWarnings("unchecked")
	@Override
	public void run() {
		int count = 1;
		try {
			while (execute) {
				try {
					new StartAccountUpdates().startUpdates();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				execute = new StartAccountUpdates().stopThreadCheck();
				Thread.sleep(30000);
				count++;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
