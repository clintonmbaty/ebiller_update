package com.ebiller_updates;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Calendar;
import java.util.Date;
import java.util.Vector;
import java.util.concurrent.CountDownLatch;

import org.quartz.JobBuilder;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SchedulerFactory;

import com.KPLC.Bills.HitKPLC;
import com.KPLC.Crone.CronExpressionsExample;
import com.KPLC.Crone.Eslip;
import com.KPLC.Crone.MyJob;

import com.KPLC.comm.DbManager;

import java.sql.Connection;

public class AutoLoadingFile implements Runnable {

    private CountDownLatch latch;

    public AutoLoadingFile(CountDownLatch latch) {
        this.latch = latch;
    }

    @Override
    public void run() {
        // TODO Auto-generated method stub
        try {

            while (true) {
                 System.out.println("Trying to update 5K accounts in every 20 seconds................");
                Connection conn=null;
                Vector v=new Vector();
                try {
                	int vcount=0;
                	conn=DbManager.getConnection();
                	String sql2="SELECT  COUNT(*)vcount FROM `account` WHERE updated_at <= NOW() - INTERVAL 20 HOUR";
                	PreparedStatement prep2=conn.prepareStatement(sql2);
                	ResultSet  rs2=prep2.executeQuery(); 
                	while(rs2.next()) {
                		vcount=rs2.getInt("vcount");
   					if(vcount>=1){
   						System.out.println("We have now found some accounts-------->>>>>>>>>>>> "+vcount);
   						String threadId="KPLC_"+Eslip.genRandom()+"_AUT";
   					 Runnable jointRun = new KPLCThread(threadId);
				        Thread t = new Thread(jointRun);
				        t.start();
   					}else {
   						System.out.println("No Account found, waiting after 20 hours again>>>>>>");
   					}
                	}
                	conn.close();
   				 }catch(Exception e) {
   					e.printStackTrace();
   				 }
                
                Thread.sleep(20000);
            } 
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    
    public class KPLCThread implements Runnable{
    	String thread_id;
    	public KPLCThread(String thread_id)
    	{
    		this.thread_id=thread_id;
    		
    	}
		@Override
		public void run() {
	    	queryFileData(this.thread_id);
		}
		
		
		public  void queryFileData(String thread_id) {
			System.out.println("Update Thread has started with ID no="+thread_id);
			 Connection conn=null;
			 Vector vector=new Vector();
			 try {
					String sql="SELECT  id,account_no FROM `account` WHERE updated_at <= NOW() - INTERVAL 20 HOUR  ORDER BY updated_at ASC LIMIT 5000"; 
					conn=DbManager.getConnection();
					PreparedStatement prep=conn.prepareStatement(sql);
					ResultSet  rs=prep.executeQuery();
					while(rs.next()) {
						String account_no=rs.getString("account_no");
					    Vector holder = new Vector();
					    holder.addElement(account_no);
					    vector.add(holder);
					}
					
					conn.close();
					int ii=0;
					conn = DbManager.getConnection();
//					conn.setAutoCommit(false);
					String sql1 = "UPDATE account set amount_due=?,due_date=?,updated=?,started=?  WHERE account_no=? OR account_no=?";
					PreparedStatement prep1 = conn.prepareStatement(sql1);
					for(int i = 0; i < vector.size(); i++) {
						Vector data = (Vector) vector.elementAt(i);
						String alias2="";
						 String res =	HitKPLC.KPLCqueryBills(data.toArray()[0].toString(),"biller","file","alias");
					String arrres[] = res.split("\\*");
					if(arrres[1].toString().equalsIgnoreCase("Notfound") || arrres[1].toString().equalsIgnoreCase("Error") || arrres[1].toString().equalsIgnoreCase("Failure")) {
					
					}else {
						prep1.setObject(1, arrres[1].toString());
						prep1.setObject(2, arrres[3].toString());
						prep1.setObject(3, "YES");
						prep1.setObject(4, "YES");
						prep1.setObject(5, arrres[4].toString());
						prep1.setObject(6, arrres[0].toString());
						prep1.addBatch();
					
					ii++;
					if (ii % 200 == 0 || ii == vector.size() ) {
						System.out.println(">===Updating===account Records--"+ii+" out of= "+vector.size()+"For Thread ID="+thread_id);
		                prep1.executeBatch(); // Execute every 50 items. 
		                prep1.clearBatch();       
		            }
					}
					 vector.removeElement(i);
					}
					
//					conn.setAutoCommit(true);
					conn.close();
				 }catch(Exception e) {
					 e.printStackTrace();
				 }
		 }
    	
    }
    
    public void fireJob() throws SchedulerException, InterruptedException {
        SchedulerFactory schedFact = new org.quartz.impl.StdSchedulerFactory();
        Scheduler scheduler = schedFact.getScheduler();
        scheduler.start();
         
        // define the job and tie it to our HelloJob class
        JobBuilder jobBuilder = JobBuilder.newJob(MyJob.class);
        JobDataMap data = new JobDataMap();
        data.put("latch", this);
         
        JobDetail jobDetail = jobBuilder.usingJobData("example", "com.javacodegeeks.quartz.QuartzSchedulerListenerExample") 
                .usingJobData(data)
                .withIdentity("myJob", "group1")
                .build();
         
        Calendar rightNow = Calendar.getInstance();
        int hour = rightNow.get(Calendar.HOUR_OF_DAY);
        int min = rightNow.get(Calendar.MINUTE);
         
        System.out.println("Current time: " + new Date());              
         
        // Tell quartz to schedule the job using our trigger
        // Fire at current time + 1 min every day

        scheduler.scheduleJob(jobDetail, CronExpressionsExample.fireAfterAMinEveryDayStartingAt(11, 29, 00, hour, min));

        latch.await();
        System.out.println("All triggers executed. Shutdown scheduler");
        scheduler.shutdown();
    }
     
    public void countDown() {
        latch.countDown();
    }
    
}
