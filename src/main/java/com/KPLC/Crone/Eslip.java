package com.KPLC.Crone;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Random;
import java.util.Vector;

import org.json.JSONObject;

import com.KPLC.Bills.HitKPLC;
import com.KPLC.comm.DESEncryption;
import com.KPLC.comm.DbManager;
import com.KPLC.comm.Error_Logs;
import com.KPLC.comm.TimeManager;

public class Eslip {
	static Connection conn=null;
	
	
	public static void startEslipCheck() {
		Vector vector2 = new Vector();
		try {
			String biller_type="";
			 conn=DbManager.getConnection();
			String sql="SELECT eslip_no,expiry_date,STATUS,created_at,\r\n" + 
					"(SELECT IF(closed_biller_type IS NULL, 'NA',closed_biller_type) FROM biller_profile B WHERE B.comp_code=A.biller_code)biller_type\r\n" + 
					"FROM eslip  A WHERE  STATUS=? AND expiry_date<=NOW() AND  biller_code IN (SELECT comp_code FROM biller_profile)";
			 PreparedStatement prep=conn.prepareStatement(sql);
			 prep.setObject(1, "Pending");
			 ResultSet rs=prep.executeQuery();
			 while(rs.next()) {
				 Vector holder = new Vector();
					holder.addElement(rs.getString("eslip_no"));
					biller_type=rs.getString("biller_type");
					if(biller_type.equalsIgnoreCase("B")) {
						holder.addElement(rs.getString("biller_type"));
					}else {
						holder.addElement("NA");
					}
					vector2.addElement(holder); 
			 }
			 conn.close();
			 if(vector2.isEmpty() || vector2.size()==0) {
				 System.out.println("No eslip to expire..............................................");
			 }else {
				 update(vector2);
				 refund(vector2);
			 }
			 vector2.clear();
		}catch(Exception e) {
			e.printStackTrace();
		}
		
		
		
	}
	
  public static void refund(Vector vector){
	  try {
	  for(int i = 0; i < vector.size(); i++) {
			 Vector data = (Vector) vector.elementAt(i);
			 String eslip_no=data.toArray()[0].toString();
			 String biller_type=data.toArray()[1].toString();
			 if(biller_type.equalsIgnoreCase("B")) {
				 double new_amount=0.0;
				 conn = DbManager.getConnection();
					String sql1="SELECT eslip_no,account_no,amount_to_pay,payer_code,biller_code,\r\n" + 
							"(SELECT amount_to_pay FROM invoice B WHERE B.invoice=A.account_no LIMIT 1)invoice_amount_to_pay\r\n" + 
							" FROM eslip_bills A WHERE eslip_no=?";
					PreparedStatement prep1 = conn.prepareStatement(sql1);
					prep1.setObject(1, eslip_no);
					ResultSet rs1=prep1.executeQuery();
					while(rs1.next()) {
						if(!rs1.getString("invoice_amount_to_pay").isEmpty()) {
						String invoice=rs1.getString("account_no");
						double amount_to_pay=rs1.getDouble("amount_to_pay");
						double invoice_amount_to_pay=rs1.getDouble("invoice_amount_to_pay");
						new_amount=amount_to_pay+invoice_amount_to_pay;
						String payer_code=rs1.getString("payer_code");
						String biller_code=rs1.getString("biller_code");
						
						String sql = "UPDATE invoice SET amount_to_pay=?,status=? WHERE invoice=? AND payer_code=? AND biller_code=?";
						PreparedStatement prep = conn.prepareStatement(sql);
						prep.setObject(1, new_amount);
						prep.setObject(2, "Pending");
						prep.setObject(3, invoice);
						prep.setObject(4, payer_code);
						prep.setObject(5, biller_code);
						prep.execute();
						
						String sql2 = "INSERT INTO invoice_note(mem_no,payer_code,biller_code,invoice_no,amount,type,created_by,reason)VALUES(?,?,?,?,?,?,?,?)";
						PreparedStatement prep2 = conn.prepareStatement(sql2);
						prep2.setObject(1, "SYSTEM_"+TimeManager.getEmailTime());
						prep2.setObject(2, payer_code);
						prep2.setObject(3, biller_code);
						prep2.setObject(4, invoice);
						prep2.setObject(5, new_amount);
						prep2.setObject(6, "DEBIT");
						prep2.setObject(7, "SYSMEM ESLIP CHECK");
						prep2.setObject(8, "ESLIP EXPIRED");
						prep2.execute();
						
						}
					}
				conn.close(); 
			 }
	  }
			 
	  }catch(Exception e) {
		  e.printStackTrace();
	  }
	  
	}
	
	public static void startUsersCheck() {
		
		Vector vector = new Vector();
		try {
			 conn=DbManager.getConnection();
			String sql="SELECT * FROM `bank_users` WHERE STATUS=? AND DATEDIFF(NOW(), last_login) >=30";
			 PreparedStatement prep=conn.prepareStatement(sql);
			 prep.setObject(1, "Active");
			 ResultSet rs=prep.executeQuery();
			 while(rs.next()) {
				 Vector holder = new Vector();
					holder.addElement(rs.getString("username"));
					vector.addElement(holder); 			
			 }
			 conn.close();
			 if(vector.isEmpty()) {
				 
			 }else {
				 updateUsers(vector); 
			 }
			 vector.clear();
		}catch(Exception e) {
			e.printStackTrace();
			Error_Logs.LogExceptions(e.toString(),"NA", e.getClass().toString(), Eslip.class.getName().toString());
		}
		
		
		
	}
public static void updateUsers(Vector vector) {
	 long startTime = System.currentTimeMillis(); // Get the start Time
     long endTime = 0;
     long time=0;
		try {
			
			int ii=0;
			conn = DbManager.getConnection();
			String sql="UPDATE bank_users SET status=?,CREATED_BY=? WHERE USERNAME=?";
			
			PreparedStatement prep1 = conn.prepareStatement(sql);
			
			 for(int i = 0; i < vector.size(); i++) {
				 Vector data = (Vector) vector.elementAt(i);
				 
				    prep1.setObject(1, "Inactive");
					prep1.setObject(2, "SCHEDULER");
				    prep1.setObject(3, data.toArray()[0].toString());
				    prep1.addBatch();
				   
				   ii++;
					if (ii % 200 == 0 || ii == vector.size() ) {
						System.out.println(">===Updating===users status--"+ii+" out of "+vector.size());
		                prep1.executeBatch(); // Execute every 50 items. 
		                prep1.clearBatch();  
		            }
					 vector.removeElement(i);
			 }
			 conn.close();
			 endTime = System.currentTimeMillis(); //Get the end Time
			 time=(endTime-startTime);
			 
			 HitKPLC.saveCroneLogs("CRONE JOB", time+"", "ESLIP UPDATE AND CHECK", vector.size()+"", "SUCCESS", startTime+"", endTime+"","NA");
		}catch(Exception e) {
			e.printStackTrace();
			Error_Logs.LogExceptions(e.toString(),"NA", e.getClass().toString(), Eslip.class.getName().toString());
		}
		
	}
	
	public static void update(Vector vector) {

		 long startTime = System.currentTimeMillis(); // Get the start Time
	        long endTime = 0;
	        long time=0;
		try {
			
			int ii=0;
			conn = DbManager.getConnection();
			String sql1 = "UPDATE eslip set status=?  WHERE eslip_no=?";
			String sql2 = "UPDATE eslip_bills set status=?  WHERE eslip_no=?";
			PreparedStatement prep1 = conn.prepareStatement(sql1);
			PreparedStatement prep2 = conn.prepareStatement(sql2);
			
			 for(int i = 0; i < vector.size(); i++) {
				 Vector data = (Vector) vector.elementAt(i);
				 
				   prep1.setObject(1, "Expired");
				   prep1.setObject(2, data.toArray()[0].toString());
				   prep1.addBatch();
				   
				   prep2.setObject(1, "Expired");
				   prep2.setObject(2, data.toArray()[0].toString());
				   prep2.addBatch();
				   
				   ii++;
					if (ii % 200 == 0 || ii == vector.size() ) {
						System.out.println(">===Updating===eslip status--"+ii+" out of "+vector.size());
		                prep1.executeBatch(); // Execute every 50 items. 
		                prep1.clearBatch();  
		                
		                prep2.executeBatch(); 
		                prep2.clearBatch(); 
		            }
					 
			 }
			 conn.close();
			 endTime = System.currentTimeMillis(); //Get the end Time
			 time=(endTime-startTime);
//			 HitKPLC.saveCroneLogs("CRONE JOB", time+"", "ESLIP UPDATE AND CHECK", vector.size()+"", "SUCCESS", startTime+"", endTime+"","NA");
		}catch(Exception e) {
			Error_Logs.LogExceptions(e.toString(),"NA", e.getClass().toString(), Eslip.class.getName().toString());
			e.printStackTrace();
		}
		
	}
	
	public static void startUpdates() {
		try {
			 Vector vector = new Vector();
			 String sql="SELECT  account_no FROM `account` WHERE updated_at <= NOW() - INTERVAL 12 HOUR  ORDER BY updated_at ASC LIMIT 10000";
			 conn=DbManager.getConnection();
				PreparedStatement prep=conn.prepareStatement(sql);
				ResultSet rs=prep.executeQuery();
				while(rs.next()) {
					Vector holder = new Vector();
					holder.addElement(rs.getString("account_no"));
					vector.addElement(holder);
				}
				conn.close();
				if(vector.isEmpty() || vector.size()==0) {
					System.out.println("No Accounts to update now waiting after 6 hours>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
				}else {
					System.out.println(" Some  Accounts Found to be updated and they are >>>>>>>>>"+vector.size());
					 Thread t3 = new Thread() {
							public void run() {
				CroneHelper.QueryBills(vector);
							}
						};
						t3.start(); 	
				}
					
	}catch(Exception e) {
		e.printStackTrace();
	}
	}
	
	public static int genRandom() {
		Random generator = new Random();
		return 1000 + generator.nextInt(9000);
	}
}
