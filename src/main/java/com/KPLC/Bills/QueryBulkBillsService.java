package com.KPLC.Bills;
import java.io.FileInputStream;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Iterator;
import java.util.Vector;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.http.ResponseEntity;

import com.KPLC.comm.DbManager;

import java.sql.Connection;



public class QueryBulkBillsService {
	static Connection conn = null;
	
	public static void KPLCService(String path,String payer_code,String biller_code,String file_id) {
		
		 Vector dataHolder=read(path);
	    	saveToDatabase(dataHolder,payer_code,biller_code,file_id);
	}
	
	 public static Vector read(String fileName)    {
	    	Vector cellVectorHolder = new Vector();
	    	try{
	    		FileInputStream myInput = new FileInputStream(fileName);
	   	    	//POIFSFileSystem myFileSystem = new POIFSFileSystem(myInput);
	            XSSFWorkbook myWorkBook = new XSSFWorkbook(myInput);
	            XSSFSheet mySheet = myWorkBook.getSheetAt(0);
	           Iterator rowIter = mySheet.rowIterator(); 
	           while(rowIter.hasNext()){
	        	  XSSFRow myRow = (XSSFRow) rowIter.next();
	        	  Iterator cellIter = myRow.cellIterator();
	        	  Vector cellStoreVector=new Vector();
	        	  while(cellIter.hasNext()){
	        		  XSSFCell myCell = (XSSFCell) cellIter.next();
	        		  myCell.setCellType(Cell.CELL_TYPE_STRING);
	        		  cellStoreVector.addElement(myCell);
	        	  }   
	        	  cellVectorHolder.addElement(cellStoreVector);
	          }
	    	}catch (Exception e){
	    		System.out.println(e.getMessage());
	    		}

	    	return cellVectorHolder;
	    }
	 
	 private static void saveToDatabase(Vector dataHolder,String payer_code,String biller_code,String file_id) {
		 try{ 
		 String account_no="";
			String account_name="";
			conn = DbManager.getConnection();
					String sql = "INSERT INTO file_data (account_no,account_name,payer_code,biller_code,file_id,chuncode)VALUES(?,?,?,?,?,?)";
					PreparedStatement prep = conn.prepareStatement(sql);
					conn.setAutoCommit(false);
					int ii=0;
					int chuncode=1;
					for (int i=0;i<dataHolder.size(); i++){
		                   Vector cellStoreVector=(Vector)dataHolder.elementAt(i);
					prep.setObject(1, cellStoreVector.toArray()[0].toString());
					prep.setObject(2, cellStoreVector.toArray()[1].toString());
					prep.setObject(3, payer_code);
					prep.setObject(4, biller_code);
					prep.setObject(5, file_id);
					prep.setObject(6, chuncode);
					prep.addBatch();
					ii++;
					if (ii % 1000 == 0 || ii == dataHolder.size()) {
						System.out.println(">inserting="+ii);
		                prep.executeBatch(); // Execute every 1000 items.
		                chuncode++;
		                conn.commit();
		                prep.clearBatch();
		               
		            }
					
		}
					
		            conn.setAutoCommit(true);
					conn.close();		
		 }
		catch(Exception e){
			System.out.println(e.getMessage());
			}
			
		}
	 
	
}
