package com.KPLC.Bills;

import java.io.File;
import java.io.IOException;

import org.ini4j.Ini;

import com.KPLC.comm.GeneralCodes;

public class ExternalFile {
	public static String getSecretKey() {
		 String key=null;
		 try {
		       Ini ini;   ini = new Ini(new File(GeneralCodes.configFilePath()));
			  key=ini.get("header", "SECRET_KEY");
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
		 return key;
	 }
	
	public static String getLogExceptionsURL() {
		 String key=null;
		 try {
		       Ini ini;   ini = new Ini(new File(GeneralCodes.configFilePath()));
			  key=ini.get("header", "ms_email_ldap");
		} catch (IOException e) {
			//System.out.println(e.getMessage());
		}
		 return key;
	 }
	
	public static String getUsbBaseUrl() {
		 String url=null;
		 try {
		       Ini ini;   ini = new Ini(new File(GeneralCodes.configFilePath()));

			  url=ini.get("header", "esb_account_fetch_url");

		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
		 return url;
	 }
	
	public static String getLogoPath() {
		 String url=null;
		 try {
		       Ini ini;   ini = new Ini(new File(GeneralCodes.configFilePath()));
			  url=ini.get("header", "Logo_Path");
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
		 return url;
	 }
	
	public static String getExcelPath() {
		 String url=null;
		 try {
		       Ini ini;   ini = new Ini(new File(GeneralCodes.configFilePath()));
			  url=ini.get("header", "Excel_Path");
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
		 return url;
	 }
	
	public static String getSMSBaseUrl() {
		 String url=null;
		 try {
		       Ini ini;   ini = new Ini(new File(GeneralCodes.configFilePath()));
			  url=ini.get("header", "sms_url_link");
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
		 return url;
	 }

	
	public static String getExcelReportURL() {
		 String url=null;
		 try {
		       Ini ini;   ini = new Ini(new File(GeneralCodes.configFilePath()));
			  url=ini.get("header", "Excel_Report");
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
		 return url;
	 }
	public static String getExcelReportURL_Team() {
		 String url=null;
		 try {
		       Ini ini;   ini = new Ini(new File(GeneralCodes.configFilePath()));
			  url=ini.get("header", "Excel_Report_Team");
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
		 return url;
	 }
	public static String getExceptionURL() {
		 String url=null;
		 try {
		       Ini ini;   ini = new Ini(new File(GeneralCodes.configFilePath()));
			  url=ini.get("header", "exception_file_url");
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
		 return url;
	 }
	public static String getMigratedExceptionURL() {
		 String url=null;
		 try {
		       Ini ini;   ini = new Ini(new File(GeneralCodes.configFilePath()));
			  url=ini.get("header", "migrated_exception_file_url");
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
		 return url;
	 }

}
