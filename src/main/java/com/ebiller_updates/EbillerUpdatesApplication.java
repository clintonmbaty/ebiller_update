package com.ebiller_updates;

import java.util.concurrent.CountDownLatch;

import javax.annotation.PostConstruct;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

import com.KPLC.Crone.EslipThread;
import com.KPLC.Crone.QuartzSchedulerCronTriggerExample;
import com.ebiller_updates.KPLC.Updates;

@SpringBootApplication
public class EbillerUpdatesApplication extends SpringBootServletInitializer{

	public static void main(String[] args) {
		SpringApplication.run(EbillerUpdatesApplication.class, args);
	}
	
	@Override
	 protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
	  return application.sources(EbillerUpdatesApplication.class);
	 }
	
	 @PostConstruct
	    private void init() {
		 CountDownLatch update = new CountDownLatch(1);
		 Runnable jointRun = new AutoLoadingFile(update);
	        Thread t = new Thread(jointRun);
	        t.start();    
	        
	    }

}
