package com.ebiller_updates.Work;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Vector;

import com.KPLC.Crone.CroneHelper;
import com.KPLC.comm.DbManager;

public class StartAccountUpdates {
	 Connection conn=null;
	
	 @SuppressWarnings("static-access")
	public  void startUpdates() {
		try {
			 Vector vector = new Vector();
			 String sql="SELECT  account_no FROM `account` WHERE updated_at <= NOW() - INTERVAL 12 HOUR  ORDER BY updated_at ASC LIMIT 10000";
			 conn=DbManager.getConnection();
				PreparedStatement prep=conn.prepareStatement(sql);
				ResultSet rs=prep.executeQuery();
				while(rs.next()) {
					Vector holder = new Vector();
					holder.addElement(rs.getString("account_no"));
					vector.addElement(holder);
				}
				conn.close();
				if(vector.isEmpty() || vector.size()==0) {
					System.out.println("No Accounts for pocess 2 to update now waiting after 12 hours>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
				}else {
				System.out.println(" Some  Accounts Found for process 2 to be updated and they are >>>>>>>>>"+vector.size());
				  
		        Thread t3 = new Thread() {
					public void run() {
				CroneHelper.QueryBills(vector);	
					}
				};
				t3.start();
				}
					
	}catch(Exception e) {
		e.printStackTrace();
	}
	}
	 
	 public boolean stopThreadCheck() {
		 try {
			 String sql="SELECT  COUNT(*)vcount FROM `account` WHERE updated_at <= NOW() - INTERVAL 12 HOUR";
			 conn=DbManager.getConnection();
				PreparedStatement prep=conn.prepareStatement(sql);
				ResultSet rs=prep.executeQuery();
				while(rs.next()) {
					if(rs.getInt("vcount")>=1) {
						return true;
					}
				}
				conn.close();
	}catch(Exception e) {
		e.printStackTrace();
		return true;
	} 
		 return false;
		 
	 }
}
