package com.KPLC.Crone;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Vector;

import com.KPLC.Bills.HitKPLC;
import com.KPLC.comm.DbManager;

public class CroneHelper {
	 static Connection conn=null;
	 
	 
	 public static void QueryBills(Vector vector) {
		 System.out.println("Accounts Passed>>>>>>>"+vector.size());
		 try {
			 int ii=0;
				conn = DbManager.getConnection();
//				conn.setAutoCommit(false);
				String sql1 = "UPDATE account set amount_due=?,due_date=?,updated=?,started=?  WHERE account_no=?";
				PreparedStatement prep1 = conn.prepareStatement(sql1);
		 for(int i = 0; i < vector.size(); i++) {
			 Vector data = (Vector) vector.elementAt(i);
			 String res =	HitKPLC.KPLCqueryBills(data.toArray()[0].toString(),"biller","file","alias");
			 String arrres[] = res.split("\\*");
				if(arrres[1].toString().equalsIgnoreCase("Notfound") || arrres[1].toString().equalsIgnoreCase("Error") || arrres[1].toString().equalsIgnoreCase("Failure")) {
//					System.out.println("KPLC Failure/Error on Account>>>>>>>"+arrres[1].toString());
				}else {
//					 System.out.println("Building batch------>>>>>>>>>>"+arrres[1].toString());
					prep1.setObject(1, arrres[1].toString());
					prep1.setObject(2, arrres[3].toString());
					prep1.setObject(3, "YES");
					prep1.setObject(4, "YES");
					prep1.setObject(5, arrres[4].toString());
					prep1.addBatch();
				
				ii++;
				if (ii % 200 == 0 || ii == vector.size() ) {
					System.out.println(">===Updating===account Records--"+ii+" out of "+vector.size());
	                prep1.executeBatch(); // Execute every 50 items. 
	                prep1.clearBatch();       
	            }
				}
				 vector.removeElement(i);
		 }
		 conn.close();
		 }catch(Exception e) {
//			 e.printStackTrace();
		 }
		 
	 }
	 
}
